

import java.util.Date
import akka.testkit._
import org.joda.time.DateTime
import org.specs2.mock.Mockito
import org.specs2.mutable.{BeforeAfter, Specification}
import org.specs2.specification.Scope

import akka.actor._

class TestKitTest extends TestKit(_system = ActorSystem()) with Specification with Mockito with DefaultTimeout with ImplicitSender {

  /**
   * @param in year,  monthOfYear,  dayOfMonth,  hourOfDay,  minuteOfHour,  secondOfMinute
   * @return
   */
  implicit def tupleToDate(in: (Int, Int, Int, Int, Int, Int)): Date = {
    {
      import in._
      new DateTime(_1, _2, _3, _4, _5, _6, 0).toDate
    }
  }

  // FTP States
  sealed trait FTPState

  case object FTPUninitialized extends FTPState

  case object FTPIdle extends FTPState

  case object FTPSyncing extends FTPState

  // FTP Events
  sealed trait FTPEvent

  case class FTPDoInit(conf: FTPConf) extends FTPEvent

  // FTP Data
  sealed trait FTPData

  case object FTPNone extends FTPData

  case class FTPInitialized(downloader: FTPDownloader, conf: FTPConf) extends FTPData

  case class FTPSynced(downloader: FTPDownloader, conf: FTPConf, newFiles: Option[List[_]] = None,
                       oldFiles: Option[List[_]] = None)
    extends FTPData

  // PA Events
  sealed trait EventR

  case object CheckForFiles extends EventR

  case class FilesReceived(files: List[String]) extends EventR

  // PA States
  sealed trait PAState

  case object Idle extends PAState

  case object WaitingForFiles extends PAState

  case object MonitoringFiles extends PAState

  //PA Data
  sealed trait PAData

  case object Uninitialized extends PAData

  case class Initialized(files: List[String]) extends PAData

  case class OldNew(oldFiles: List[String], newFiles: List[String]) extends PAData


  class FTPConf(val remoteDir: String = "/") {}

  class FTPDownloader(val conf: FTPConf) {

    def open() = ()

    def changeDir(s: String) = ()


  }


  class BaseScope extends Scope with BeforeAfter {

    //    val ftpRef =  TestActorRef(new FTPFetcher {
    //      override def getParent() = testActor
    //    })
    //
    //    //    val wrapper = system.actorOf(Props(new Actor {
    //    //      var child = context.actorOf(Props[FTPFetcher])
    //    //      ftpRef = child
    //    //      def receive = {
    //    //        case x => if (sender == child) testActor forward x else child forward x
    //    //      }
    //    //    }))
    //
    //    var tmpdir = new TemporaryFolder
    //    tmpdir.create()
    //    // val paingest = TestFSMRef(new PaIngestActor(getJobService()))
    //    def before = server = createServer("", "", 2221, "/", List[FileEntry]())
    //    def after: Any = {
    //      server.stop();
    //      tmpdir.delete()
    //    }

    class FTPFsm extends Actor with FSM[FTPState, FTPData] {

      startWith(FTPUninitialized, FTPNone)

      when(FTPUninitialized) {
        case Event(FTPDoInit(c), FTPNone) => {
          val ftp = new FTPDownloader(c)
          ftp.open()
          ftp.changeDir(c.remoteDir)
          goto(FTPIdle) using FTPInitialized(ftp, c)
        }
      }

      when(FTPIdle) {
        case Event('checkfiles, input@FTPInitialized(ftp, c)) => goto(FTPIdle) using input
      }

      when(FTPSyncing) {
        //    case Event('checkfiles, state @ FTPInitialized(ftp)) =>
        //      goto(FTPSyncing) using state
        case _ => throw new UnsupportedOperationException
      }

      onTransition {
        case FTPUninitialized -> FTPIdle =>
          stateData match {
            case _ => {
              print("replying!")
              context.parent ! 'connected
            }
          }
      }
    }


    def after = println("after")

    def before = println("before")
  }

  class TestScope1 extends BaseScope {
  }

  class TestScope2 extends BaseScope {
  }

  class TestScope3 extends BaseScope {
  }

  class TestScope4 extends BaseScope {
  }

  class TestScope5 extends BaseScope {
    println("FDSAFDSAFDDSAAFDS")
  }

  "FTPFetcher should " in {
    sequential

    "Verify the initial state" in new TestScope1 {

      class FTP extends Actor {
        def receive = {
          case a => context.parent ! a
        }
      }

      testActor ! 'blah
      expectMsg('blah)

      var f: FTPFsm = _
      val p = system.actorOf(Props(new Actor {
        val child = context.actorOf(Props({
          f = new FTPFsm(); f
        }), "child")
        def receive = {
          case x if sender == child => testActor forward x
          case x => child forward x
        }
      }))

      p ! FTPDoInit(new FTPConf)
      p ! 'checkfiles
      expectMsg('connected)

      f.stateName must_== FTPIdle

      success
    }

//    "Change state correctly " in new TestScope2 {
//      import akka.testkit._
//      val t = TestFSMRef(new FTPFsm())
//      t ! FTPDoInit(new FTPConf)
//      t ! 'checkfiles
//      expectMsg('connected)
//      success
//    }

    "Ensure files are processed in the correct order, i.e. earliest first" in new TestScope3 {
      true must_== true
    }

    "If a new file is found, it should be ingested first, then each newer file to the most recent file" in
      new TestScope4 {
        true must_== true
      }

    "File ingest order will be determined by date encoded in file-name, not the file-modification time" in
      new TestScope5 {
        true must_== true
      }
  }
}
