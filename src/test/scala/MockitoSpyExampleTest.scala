
import org.specs2.mock.mockito.CalledMatchers
import org.specs2.mock.MockitoMocker
import org.specs2.mutable.SpecificationWithJUnit

/*
*
 */
class MockitoSpyExampleTest extends SpecificationWithJUnit with MockitoMocker with CalledMatchers {


  "Testing the behavior of Mockito spy instances in a simple case" should {

    "Modify one method, but leave the second (calling) method unchanged " >> {

      class Foo {
        def internalGetNumber = 15

        def externalGetNumber = internalGetNumber
      }

      val foo = spy(new Foo)
      doReturn(20).when(foo).internalGetNumber


      foo.externalGetNumber must_== 20

      there was one(foo).internalGetNumber

      success
    }

  }


}

