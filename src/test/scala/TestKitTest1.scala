
import akka.actor._
import akka.actor.FSM
import akka.testkit._
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

class TestKitTest1 extends TestKit(_system = ActorSystem()) with Specification with Mockito with DefaultTimeout with ImplicitSender {

  // FTP States
  sealed trait FTPState

  case object FTPUninitialized extends FTPState

  case object FTPIdle extends FTPState

  case object FTPSyncing extends FTPState

  // FTP Events
  sealed trait FTPEvent

  case class FTPDoInit(conf: FTPConf) extends FTPEvent

  // FTP Data
  sealed trait FTPData

  case object FTPNone extends FTPData

  case class FTPInitialized(downloader: FTPDownloader, conf: FTPConf) extends FTPData

  case class FTPSynced(downloader: FTPDownloader, conf: FTPConf, newFiles: Option[List[_]] = None,
                       oldFiles: Option[List[_]] = None) extends FTPData

  class FTPConf(val remoteDir: String = "/") {}

  class FTPDownloader(val conf: FTPConf) {
    def open() = ()
    def changeDir(s: String) = ()
  }

  "FTPFetcher should " in {
    "Verify Parent - Child communications" in {
      class FTPFsm extends Actor with FSM[FTPState, FTPData] {
        startWith(FTPUninitialized, FTPNone)

        when(FTPUninitialized) {
          case Event(FTPDoInit(c), FTPNone) => {
            println("FTPDoInit")
            goto(FTPIdle) using FTPInitialized(new FTPDownloader(c), c)
          }
        }

        when(FTPIdle) {
          case Event('checkfiles, input@FTPInitialized(ftp, c)) => goto(FTPIdle) using input
        }

        onTransition {
          case FTPUninitialized -> FTPIdle =>
            stateData match {
              case _ => {
                print("replying!")
                context.parent ! 'connected
              }
            }
        }
      }

      val p = system.actorOf(Props(new Actor {
        val child = context.actorOf(Props(new FTPFsm), "child")

        def receive = {
          case x if sender == child => testActor forward x
          case x => child forward x
        }
      }))

      p ! FTPDoInit(new FTPConf)
      p ! 'checkfiles
      expectMsg('connected)
      success
    }
  }
}
