object ScratchMain {
  def main(args: Array[String]) = {
    import scala.collection._
    class MyColl[A](seq: A*) extends Traversable[A] {
      // only abstract method in traversable is foreach... easy :)
      def foreach[U](f: A => U) = util.Random.shuffle(seq.toSeq).foreach(f)
    }
    val c = new MyColl(1, 2, 3)
    println(c mkString ",")
    println(c drop 1 mkString ",")
  }
}
object ScratchMain2 {
  def main(args: Array[String]) {
    import scala.collection._
    import scala.collection.generic._
    // The TraversableFactory trait is required by GenericTraversableTemplate
    object MyColl extends TraversableFactory[MyColl] {
      /*
      If you look at the signatures of many methods in TraversableLike they have an
      implicit parameter canBuildFrom.  This allows one to define how the returned collections
      are built.  For example one could make a list's map method return a Set
      In this case we define the default canBuildFrom for MyColl
      */
      implicit def canBuildFrom[A]: CanBuildFrom[Coll, A, MyColl[A]] = new GenericCanBuildFrom[A]
      /*
      The method that builds the new collection.  This is a simple implementation
      but it works.  There are other implementations to assist with implementation if
      needed
      */
      def newBuilder[A] = new scala.collection.mutable.LazyBuilder[A, MyColl[A]] {
        def result = {
          val data = parts.foldLeft(List[A]()) {
            (l, n) => l ++ n
          }
          new MyColl(data: _*)
        }
      }
    }
    /*
    Adding GenericTraversableTemplate will delegate the creation of new
    collections to the companion object.  Adding the trait and
    companion object causes all the new collections to be instances of MyColl
    */
    class MyColl[A](seq: A*) extends Traversable[A] with GenericTraversableTemplate[A, MyColl] {
      override def companion = {
        MyColl
      }
      def foreach[U](f: A => U) = util.Random.shuffle(seq.toSeq).foreach(f)
    }
    val c = new MyColl(1, 2, 3)
    println(c mkString ",")
    println(c drop 1 mkString ",")
    assert(c.drop(1).isInstanceOf[MyColl[_]])
  }
}
