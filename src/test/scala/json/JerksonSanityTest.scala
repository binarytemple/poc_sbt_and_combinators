package json

import org.specs2.mutable.Specification
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.specs2.mock.Mockito
import org.specs2.specification.Scope
import com.codahale.jerkson._
import com.codahale.jerkson.AST._
import com.codahale.jerkson.AST.JString
import com.codahale.jerkson.AST.JInt
import com.codahale.jerkson.AST.JArray
import org.specs2.matcher.ContentMatchers

@RunWith(classOf[JUnitRunner])
class JerksonSanityTest extends Specification with Mockito with ContentMatchers {


  trait JerksonScope extends Scope {
    val personJSON = """{"id":1,"name":"dave"}"""
    val groupJSON = """{"id":1,"name":"dave","members":["john","andrew"]}"""
  }

  "Jerkson " should {

    "Parse JValue's " in new JerksonScope {

      val groupJVal = Json.parse[JValue](groupJSON)
      val personJVal = Json.parse[JValue](personJSON)

      System.err.println(personJVal \ "id")
      System.err.println(personJVal \ "name")
      System.err.println(groupJVal \\ "members")

      personJVal \ "id" must_== JInt(1)
      personJVal \ "name" must_== JString("dave")
      //(groupJVal \\ "members" ).toArray(0)  should_== JArray(List(JString("john"), JString("andrew")))
//      groupJVal \\ 
      System.out.println(groupJVal \\ "members")


    }

    "Parse case classes " in new JerksonScope {

      val p: Person = Json.parse[Person](personJSON)
      //val g: Group = Json.parse[Group](groupJSON)

      //res5: com.codahale.jerkson.AST.JValue = JObject(List(JField(dog,JString(dave))))
      //res7: com.codahale.jerkson.AST.JValue = JObject(List(JField(dog,JString(dave)), JField(pete,JArray(List(JString(john), JString(andrew))))))

      success

    }


  }
}
