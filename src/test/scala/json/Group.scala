package json

case class Group(id: Int, name: String, members: Option[List[String]])
