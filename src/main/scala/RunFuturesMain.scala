

object RunFuturesMain extends App {

  import concurrent.{ExecutionContext, Future, Await}
  import concurrent.duration.Duration
  import java.util.concurrent.Executors
  import akka.actor.ActorSystem
  import java.net.URL
  import akka.dispatch.ExecutionContexts.global
  import org.apache.commons.io.IOUtils

  implicit val as = ActorSystem.create("default")
  implicit var ec = global

  val seconds_5 = Duration.create(5, "seconds")
  val seconds_20 = Duration.create(20, "seconds")

  val urls = List("http://yahoo.com", "http://google.com", "http://zeebox.com").map(x => Future {
    val u = new URL(x)
    IOUtils.toString(u.openStream())
  })

  val sequence: Future[List[String]] = Future.sequence(urls)

  val res = Await.result(sequence, seconds_5)
  println(res)
  as.shutdown()


  // --- now with custom thread pool (execution context)

  System.err.println("Now switching to custom thread pool")

  //val pool = Executors.newCachedThreadPool()
  val pool = Executors.newFixedThreadPool(3)
  ec = ExecutionContext.fromExecutorService(pool)

  val sequence2: Future[List[String]] = Future.sequence(urls)

  val res2 = Await.result(sequence2, seconds_20)
  println(res2)
  pool.shutdown()

}
