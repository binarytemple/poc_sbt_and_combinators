import akka.dataflow._
import scala.concurrent.ExecutionContext.Implicits.global
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

// Multiple flows, executing in random order
object DataflowMain extends App {
  val latch = new CountDownLatch(3)

  flow { "Hello world 1!" } onComplete {
    a => { println(a); latch.countDown() }
  }

  flow { "Hello world 2!" } onComplete {
    a => { println(a); latch.countDown() }
  }

  // A composite flow
  flow {
    val f1 = flow { "Hello" }
    f1() + " world!"
  } onComplete {
    a => { println(a); latch.countDown() }
  }

  latch.await(10, TimeUnit.SECONDS)
}