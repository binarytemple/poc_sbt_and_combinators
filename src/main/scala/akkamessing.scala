import akka.actor.{Props, ActorSystem, Actor}
import reflect.{ClassTag, ClassManifest}

class Parent[T <: Child : ClassTag] extends Actor {
  val child = context.actorOf(Props[T])

  def receive = {
    case 'pong => println("got pong")
    case 'ping_child => child ! 'ping
    case a => println(a)
  }
}

class Child extends Actor {
  def receive = {
    case 'ping => context.parent ! 'pong
    case a => println(a)
  }
}

object Boot {
  def main(args: Array[String]) = {
    val as = ActorSystem("as")
    val p = as.actorOf(Props(new Parent[Child]()))
    p ! 'test
    p ! 'pong
    p ! 'pong_child
    as.shutdown()
    as.awaitTermination()
  }
}