

sealed abstract trait BaseSearch
case class BoogleSearch(term: String) extends BaseSearch
case class IfunesSearch(term: String, name: String) extends BaseSearch

abstract class AbstractSearcher[T <: BaseSearch] {
  def search(s: T)
}

class BoogleSearcher extends AbstractSearcher[BoogleSearch] {
  def search(s: BoogleSearch) {
    println(s"searching with class $s")
  }
}

class IfunesSearcher extends AbstractSearcher[IfunesSearch] {
  def search(s: IfunesSearch) {
    println(s"searching with class $s")
  }
}

object InheritOverride {
  def main(args: Array[String]) = {
    println("Inheritence and method override example")
    new BoogleSearcher().search(new BoogleSearch("asfd"))
    new IfunesSearcher().search(new IfunesSearch("asfd", "asf"))
  }
}
