
import scala.util.parsing.combinator.lexical.StdLexical
import scala.util.parsing.combinator.syntactical.StandardTokenParsers
object ExprParser1 extends StandardTokenParsers {

    lexical.delimiters ++= List("\"","(",")")
  
    def key = stringLit ^^ { s => s }
    def word = numericLit ^^ { s => s }
    def keys =  ( key * ) 
    def words =  ( word * ) 
    def sentence = keys ~ words 
    def parens:Parser[Any] = "(" ~> ( sentence ~ ( parens * )) <~ ")" ^^ { s=> s }
    
    def parse(s:String) = {
        val tokens = new lexical.Scanner(s)
        phrase(parens)(tokens)
    }

    def apply(s:String):Any = {
        parse(s) match {
            case Success(tree, _) => tree
            case e: NoSuccess =>
                   throw new IllegalArgumentException("Bad syntax: "+s)
        }
    }
    //Simplify testing
    def test(exprstr: String) = {
      println(lexical.delimiters)
      parse(exprstr) match {
            case Success(tree, _) =>
                println("Tree: "+tree)
                //val v = tree.eval()
                //println("Eval: "+v)
            case e: NoSuccess => Console.err.println(e)
        }
    }
    //A main method for testing
    def main(args: Array[String]) = test(args(0))
}

//ExprParser.test(""" 
//( "a" "b" "abcd" 1 3 3 4 4 ( "AND" 2 ) ("OR" 5 ("g" 5 ) ))
//""")

// vim: set ts=2 sw=2 et:
