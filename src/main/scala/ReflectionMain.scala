

import scala.reflect.runtime.{universe => ru, currentMirror => m}


object Blahda {

  def main(a:Array[String]) = {

    val l = List(1,2,3)

    def getTypeTag[T: ru.TypeTag](obj: T) = ru.typeTag[T]

    val theType = getTypeTag(l).tpe

    print(theType)
  }



}

//class Foo(x: Int, val y: Int) {
//	  def this(x: Int) = this(x, 0)
//	  def <<(z: Int) {}
//	}
//	import scala.reflect.runtime.universe._
//	println(typeOf[List[Int]])
//	println(typeOf[Either[String, Int]])
//	println(reify(new Foo(1,2)))
//	reify(new Foo(1,2)).tree.children.foreach(t=> println(t))

