
/**
* https://github.com/spray/spray-json
*/
object SprayExample {
  def main(args: Array[String]) {
    import spray.json._
    case class Blah(s: String, i: Int)
    case class Blah4(s: String, i: Int,j: Int,k: Int)
    object MyJsonProtocol extends DefaultJsonProtocol {
      /////////////////////////////////////^Match the param length
      implicit val blah2Format = jsonFormat2(Blah)
      implicit val blah4Format = jsonFormat4(Blah4)
    }
    import MyJsonProtocol._
    println(new Blah("sda", 10).toJson)
    println(new Blah4("sda", 10,1,2).toJson)
    println(new Blah4("sda", 10,1,2).toString)
  }
}
